## Our Services

### Manage Accounting, Employment, Contracting, Payroll 

Our web platform allows for all projects under our roof to manage and
allocate their budget as they see fit, including provisions for
sub-budgets such as contract work, travel, or equipment. Our platform
will facilitate all transactions and help us manage required paperwork
including receipts and documentation, will provide employment and
contracting templates, both for Europe and world-wide, according to the
specific needs and wishes of projects, employees, and contractors. We
manage payroll and tax reporting, and provide necessary documents
for internal and external audits. In essence, all projects are free
to allocate funds and work as they wish, and we are here to take care that
everything goes smoothly and is properly documented.

### Manage Donations, Donor Reporting, and Help with Fundraising

We support our projects' applications for funds (for example to private
foundations or for EU grants) and will also spearhead our own funding
initiatives. The donor management platform will facilitate
tax-deductible donations to all projects under our roof using various
means such as bank transfer, Paypal, or Bitcoin. We take care of
all matters accounting, and in cooperation with the projects provide the
necessary documentation for donors. This includes the processing of
larger grants, including the management of (restricted) funds, the joint
handling of deadlines and deliverables, and all necessary reporting.

Initially, the Center will be able to provide instant tax-deductibility
in Germany and within some European member states. We are confident that
by partnering with fiscal sponsors in other regions of the world, we
will be able to offer tax-deductibility in more and more regions of the
world over time.

### Provide Asset Stewardship

We are able to hold assets, e.g. trademarks, domain names, and
physical equipment, for member projects whenever they would like us to
do so. In many cases, this can help avoid licensing issues quite common
in larger projects with fluctuating contributors.

### Offer an Expert Network and Mentor Community

Our own knowledge and connections and the knowledge and connections of
our projects combine to build an expert network. We use this to provide
advice to projects, but also to help fill needs and vacancies, to
connect developers and activists with, for example, designers,
journalists and institutions. All projects will be able to call upon our
mentor community (including members of our advisory board and other
experts) with problems or questions they might have.

### Facilitate In-Person Developer Meetings / Code Retreats / Hackathons

Most open source projects are developed in a distributed fashion. With
some projects, it is quite common that developer teams have never met in
person. Despite this, the importance and effectiveness of human contact
is as strong as ever. Most successful projects have in common the fact
they regularly meet, e.g. at project-specific developer conferences,
which often see great bursts of creativity and productivity. We organize
and plan these meetings, including travel arrangements and conference
support (moderating, note-taking, etc.) We also actively seek to bring
individuals and projects under our roof together with other developers
and activists.

### Provide & arrange office space

While most projects are not housed in a specific location, where this is
the case and appropriate, we help arrange office space, ideally shared
office space with similar projects to enable communication and further
collaboration. We are already providing such a space to the community in
Berlin.

### Offer legal support

This ranges from helping with the 'spin-off' of individual projects as
their own organizations, to legally defending our projects and
affiliated interests in court.

### Take care of other organizational matters

From (virtual) phone networks to postal mail services (scanning and
encrypted delivery to the projects), we also take care of all matters
"office".
